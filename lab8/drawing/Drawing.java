package drawing;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();

	
	public double totalArea(){
		double totalArea = 0;
		
		for(Shape shape: shapes){
			if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				totalArea += rect.area();
			}
			else if(shape instanceof Circle){
				Circle circle = (Circle) shape;
				totalArea += circle.area();
				
			}
			else if(shape instanceof Square){
				Square sq = (Square) shape;
				totalArea += sq.area();
			}
		}
		return totalArea;
	}
	
	
	public void draw(){
		for(Shape shape: shapes){
			if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				rect.draw();
				
			}
			else if(shape instanceof Circle){
				Circle circle = (Circle) shape;
				circle.draw();
			
		}
			else if(shape instanceof Square){
				Square sq = (Square) shape;
				sq.draw();
			}
			
	}
	}
	
	public void move(int xDistance,  int yDistance){
		for(Shape shape: shapes){
			if(shape instanceof Square){
				Square sq = (Square) shape;
				sq.move(xDistance, yDistance);
			}
			else if(shape instanceof Circle){
				Circle circle = (Circle) shape;
				circle.move(xDistance, yDistance);
		}
			else if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				rect.move(xDistance, yDistance);
				
			}

	}
	}
	
	public void addCircle(Shape circle){
		shapes.add(circle);
	}
	
	public void addRectangle(Shape rect){
		shapes.add(rect);
	}
	public void addSquare(Shape sq){
		shapes.add(sq);
	}
	
}