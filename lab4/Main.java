
public class Main {
	public static void main(String[] args){
		Rectangle rect1 = new Rectangle();
		rect1.sideA = 5;
		rect1.sideB = 6;
		int rectArea = rect1.area();
		
		System.out.println(rectArea);
		
		
		Rectangle rect2 = new Rectangle();
		rect2.sideA = 3;
		rect2.sideB = 4;
		
		System.out.println(rect1.area());
		System.out.println(rect2.area());
		System.out.println(rect1.perimeter());
		System.out.println(rect2.perimeter());
		
		Rectangle [] rects = new Rectangle[2];
		rects[0] = rect1;
		
		System.out.println(rects[0].area());
		
		
		Circle circle = new Circle(10);
		
		System.out.println(circle.area());
		
		
	}

}
