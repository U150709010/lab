package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	ArrayList<T> stack = new ArrayList<T>();
	
	
	public void push(T item) {
		stack.add(0, item);
	}

	public T pop() {		
		return stack.remove(0);
	}

	public boolean empty() {
		return stack.isEmpty();
	}

	public List<T> toList() {
		
		return stack;
	}

	public void addAll(Stack<T> aStack) {
		stack.addAll(0, aStack.toList());
		
	}
}
