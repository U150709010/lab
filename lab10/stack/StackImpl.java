package stack;
import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {

	StackItem<T> top = null;
	
	public void push(T item) {
		StackItem<T> newTop = new StackItem<T>(item);
		newTop.setNext(top);
		top = newTop;
	}

	public T pop() {
		T item = top.getItem();
		top = top.getNext();
		return item;
	}

	public boolean empty() {
		return (top == null);
	}

	public List<T> toList() {
		List<T> lst = new ArrayList<T>();
		StackItem<T> current = top;
		while(current!=null){
			lst.add(current.getItem());
			current = current.getNext();
		}
		
		return lst;
	}

	public void addAll(Stack<T> aStack) {
		for(int i = aStack.toList().size()-1; i>=0; i--){
			push(aStack.toList().get(i));
		}
		
	}
	
}