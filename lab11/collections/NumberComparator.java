package collections;

import java.util.Comparator;

public class NumberComparator implements Comparator<String> {

	@Override
	public int compare(String arg0, String arg1) {
		Integer integer0 = Integer.parseInt(arg0);
		Integer integer1 = Integer.parseInt(arg1);
		return integer0 - integer1;
	}

}
