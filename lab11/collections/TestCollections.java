package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class TestCollections {

	public static void main(String[] args) {
		String values = "23 4 6 12 5 4 16 12 6";
		String [] a = values.split(" ");
		
		Collection<String> collections = new ArrayList<>();
			
		for(String string : a){
			collections.add(string);
			
		}
		
		System.out.println(collections);
		
		
		collections.add("4");
		
		
		System.out.println(collections);
		
		
		Set<String> set = new HashSet<>();
		
		for(String string : a){
			set.add(string);
			
		}
		
		System.out.println("HashSet: " + set);
		
		set.add("15");
		
		System.out.println("HashSet: " + set);
		
		
		Set<String> linkedSet = new LinkedHashSet<>();
		
		for(String string : a){
			linkedSet.add(string);
			
		}
		System.out.println("LinkedHashSet: " + linkedSet);
		
		linkedSet.add("15");

		System.out.println("LinkedHashSet: " + linkedSet);
		
		
		
		Set<String> treeSet = new TreeSet<>();
		
		for(String string : a){
			treeSet.add(string);
			
		}
		System.out.println("TreeSet: " + treeSet);

		treeSet.add("15");

		System.out.println("TreeSet: " + treeSet);
		
		Set<String> treeSet2 = new TreeSet<>(new NumberComparator());
		
		for(String string : a){
			treeSet2.add(string);
			
		}

		System.out.println("TreeSet Comps: " + treeSet2);

		treeSet2.add("15");

		System.out.println("TreeSet  Comp: " + treeSet2);
		
		Queue<String> queue = new LinkedList<>();

		for (String string: a) {
			queue.add(string);
		}

		System.out.println("Queue: " + queue);

		System.out.println("removed: " + queue.remove());

		System.out.println("Queue: " + queue);
		
		
		Queue<String> pQueue = new PriorityQueue<>();

		for (String string : a) {
			pQueue.add(string);
		}

		System.out.println("Priority Queue: " + pQueue);

		System.out.println("removed: " + pQueue.remove());

		System.out.println("Priority Queue: " + pQueue);
		
		
		
		Queue<String> pComQueue = new PriorityQueue<>(new NumberComparator());
		
		for (String string : a) {
			pComQueue.add(string);
		}
		
		
		System.out.println("Priority Queue Comp: " + pComQueue);

		System.out.println("removed: " + pComQueue.remove());

		System.out.println("Priority Queue Comp: " + pComQueue);
		
		
		Queue<String> stack = Collections.asLifoQueue(new LinkedList<String>());

		for (String string : a) {
			stack.add(string);
		}

		System.out.println("Stack Comp: " + stack);

		System.out.println("removed: " + stack.remove());

		System.out.println("Stack  Comp: " + stack);
		




		

		
		


	}

}
