package onder.main;
import onder.shapes.*;
import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args){
		Circle circle = new Circle(6);

		
		System.out.println(circle.area());
		System.out.println();

		
		ArrayList <Circle> circles = new ArrayList();
		circles.add(circle);
		circles.add(new Circle(7));
		circles.add(new Circle(8));
		circles.add(new Circle(9));
		
		for(int i = 0; i< circles.size(); i++){
			Circle circ = circles.get(i);
			System.out.println(circ.area());
		}
	}

}
