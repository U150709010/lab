package onder.shapes;

public class Rectangle {
	protected int side1;
	protected int side2;
	public Rectangle(int side1, int side2) {
		this.side1 = side1;
		this.side2 = side2;
	}
	
	public double area(){
		return side1*side2;
	}

}
