package midterm;

public class Cat extends Animal{
	String name;

	public Cat(String name) {
		this.name = name;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String speak() {
		
		return "meow";
	}


}
