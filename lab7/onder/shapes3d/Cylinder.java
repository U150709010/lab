package onder.shapes3d;

import onder.shapes.Circle;

public class Cylinder extends Circle {

	
	public Cylinder(int radius) {
		super(radius);

	}
	private int height;
	public Cylinder(int r,int h) {
		super(r);
		height = h;
	}

	
	public double area(){
		return 2 * super.area() +height * 2 * Math.PI * getRadius();
	}
	public double volume(){
		return super.area() * height;
	}
	
}
